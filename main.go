// Copyright (c) OpenFaaS Author(s) 2020. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/adshao/go-binance/v2"
	"github.com/openfaas-incubator/nats-connector/config"
	"github.com/openfaas-incubator/nats-connector/nats"
)

type Event struct {
	Topic   string
	Message string
	Symbol  string
}

var publishChannel chan Event

func main() {
	config := config.Get()

	brokerConfig := nats.BrokerConfig{
		Host:        config.Broker,
		ConnTimeout: config.UpstreamTimeout, // ConnTimeout isn't the same as UpstreamTimeout, it's just the delay to connect to NATS.
	}

	fmt.Printf(`==============================================================================
Binance Connector for OpenFaaS

NATS URL: nats://%s:4222
Upstream timeout: %s
Symbols: %s
Symbols delimeter: %s
Env: %s
Topic prefix: %s
==============================================================================

`, config.Broker,
		config.UpstreamTimeout,
		config.Symbols,
		config.SymbolsDelimiter,
		config.Env,
		config.TopicPrefix,
	)

	broker, err := nats.NewBroker(brokerConfig)

	if err != nil {
		log.Fatal(err)
	}
	defer broker.Close()

	publishChannel = make(chan Event)
	defer close(publishChannel)

	/*******************
	*
	* define handler functions
	*
	**********************/

	wsTradeHandler := func(event *binance.WsTradeEvent) {
		jsonEvent, err := json.Marshal(event)
		if err != nil {
			log.Println(err)
			return
		}
		publishChannel <- Event{"crypto-trade", string(jsonEvent), event.Symbol}
	}

	wsAggTradeHandler := func(event *binance.WsAggTradeEvent) {
		jsonEvent, err := json.Marshal(event)
		if err != nil {
			log.Println(err)
			return
		}
		publishChannel <- Event{"crypto-aggtrade", string(jsonEvent), event.Symbol}
	}

	wsKlineHandler := func(event *binance.WsKlineEvent) {
		jsonEvent, err := json.Marshal(event)
		if err != nil {
			log.Println(err)
			return
		}
		publishChannel <- Event{"crypto-kline", string(jsonEvent), event.Symbol}
	}

	wsDepthHandler := func(event *binance.WsDepthEvent) {
		jsonEvent, err := json.Marshal(event)
		if err != nil {
			log.Println(err)
			return
		}
		publishChannel <- Event{"crypto-depth", string(jsonEvent), event.Symbol}
	}

	/*******************
	*
	* subscribe to websockets
	*
	**********************/

	_, err = broker.SubscribeTrade(wsTradeHandler, config.Symbols)

	if err != nil {
		log.Fatal(err)
	}

	_, err = broker.SubscribeAggTrade(wsAggTradeHandler, config.Symbols)

	if err != nil {
		log.Fatal(err)
	}

	_, err = broker.SubscribeKline(wsKlineHandler, "1m", config.Symbols)

	if err != nil {
		log.Fatal(err)
	}

	_, err = broker.SubscribeDepth(wsDepthHandler, config.Symbols)

	if err != nil {
		log.Fatal(err)
	}

	for {
		event := <-publishChannel
		fmt.Printf("%s: %s - %s\n", time.Now().String(), event.Topic, event.Symbol)
		err = broker.Publish(config.TopicPrefix+event.Topic, event.Message)
		if err != nil {
			log.Println(err)
		}
	}
}
