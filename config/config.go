// Copyright (c) OpenFaaS Author(s) 2020. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

package config

import (
	"log"
	"os"
	"strings"
	"time"
)

const defaultUpstreamTimeout = time.Second * 60

// Config for the NATS Connector
type Config struct {
	Broker           string
	Symbols          []string
	SymbolsDelimiter string
	UpstreamTimeout  time.Duration
	Env              string
	TopicPrefix      string
}

// Get will load the NATS Connector config from the environment variables
func Get() Config {
	broker := "nats"
	if val, exists := os.LookupEnv("broker_host"); exists {
		broker = val
	}

	symbols := []string{}
	if val, exists := os.LookupEnv("symbols"); exists {
		for _, symbol := range strings.Split(val, ",") {
			if len(symbol) > 0 {
				symbols = append(symbols, symbol)
			}
		}
	}

	if len(symbols) == 0 {
		log.Fatal(`Provide a list of topics i.e. symbols="btcbusd,bnbbtc"`)
	}

	symbols_delimiter := ","
	if val, exists := os.LookupEnv("symbols_delimiter"); exists {
		if len(val) > 0 {
			symbols_delimiter = val
		}
	}

	upstreamTimeout := defaultUpstreamTimeout

	if val, exists := os.LookupEnv("upstream_timeout"); exists {
		parsedVal, err := time.ParseDuration(val)
		if err == nil {
			upstreamTimeout = parsedVal
		}
	}

	env := "dev"
	topicPrefix := "dev-"
	if val, exists := os.LookupEnv("env"); exists {
		if len(val) > 0 {
			env = val
			topicPrefix = env + "-"
			if val == "prod" {
				topicPrefix = ""
			}
		}
	}
	return Config{
		Broker:           broker,
		Symbols:          symbols,
		SymbolsDelimiter: symbols_delimiter,
		UpstreamTimeout:  upstreamTimeout,
		Env:              env,
		TopicPrefix:      topicPrefix,
	}
}
